config = {
    
    #Login Details
    'server': 'irc.twitch.tv',
    'port': 6667,
    'username': 'luggagebot',
    'oauth_password': 'oauth:fruq6mz0v52jj46szfhrsfcgjskg2s',
    
    #Channels to join
    'channels': ['#toffster96'],

    #Set to True to return all received data
    'debug': False,

    #Cron data
    'cron': {
        '#kattcha': {
            'run_cron': False,
            'run_time': 10,
            'cron_messages': [
                'Example cron process return value 1',
                'Example cron process return value 2'
            ]
        },
    },

    #Maximum number of bytes to recieved from socket (1024-4096 recommended)
    'socket_buffer_size': 4096,

    #Feature toggles
    'chat_bot': False,

    'permit_timeout': 10,

    'pointsInterval': 300,

    'pointsPer': 10,

    'leaderboardSize': 3,

    'welcomeMode': False,

}