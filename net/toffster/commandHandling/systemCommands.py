# Import libraries
import importlib
import time
import json
from tkinter import END

import net.toffster.commandHandling.commandHeaders as commandHeaders
import net.toffster.data.config as config

# Define colours
red = "\033[01;31m{0}\033[00m"
cya = "\033[01;36m{0}\033[00m"
blu = "\033[01;34m{0}\033[00m"


def pp(message, mtype='INFO'):
    mtype = mtype.upper()

    if mtype == 'ERROR':
        mtype = red.format(mtype)
    elif mtype == 'DEBUG':
        mtype = cya.format(mtype)
    elif mtype == 'INFO':
        mtype = blu.format(mtype)
    elif mtype == "MSG":
        mtype = cya.format(mtype)

    print('[%s] [%s] %s' % (time.strftime('%H:%M:%S', time.gmtime()), mtype, message))


def jpp(data):
    if data:
        return print(json.dumps(data, indent=4, sort_keys=True) + '\n')
    else:
        return "Internal error! Incorrect usage of JPP!"


def epp(message, irc, channel=''):
    irc.send_message(channel, message)


def cpp(message, gui, mtype='INFO'):
    mtype = mtype.upper()
    console = gui.get_console()

    if mtype.upper() == "DEBUG" and not config.config['debug']:
        return False

    console.insert(END, '\n' + '[' + time.strftime('%H:%M:%S', time.gmtime()) + ']' + '[' + mtype + '] ' + message)


def is_valid_command(command):
    if command.lower() in commandHeaders.commands:
        return True


def update_last_used(command):
    commandHeaders.commands[command]['last_used'] = time.time()


def get_command_limit(command):
    return commandHeaders.commands[command]['limit']


def is_on_cooldown(command):
    command = command.lower()
    if time.time() - commandHeaders.commands[command]['last_used'] < commandHeaders.commands[command.lower()][
        'limit']:
        return True


def get_cooldown_remaining(command):
    return round(commandHeaders.commands[command]['limit'] - (
        time.time() - commandHeaders.commands[command]['last_used']))


def check_has_return(command):
    if commandHeaders.commands[command]['return'] and commandHeaders.commands[command]['return'] != 'command':
        return True


def get_return(command):
    return commandHeaders.commands[command]['return']


def check_has_args(command):
    try:
        if 'argc' in commandHeaders.commands[command]:
            return True
    except Exception:
        pp("Argument count is not defined!", "ERROR")


def check_has_correct_args(message, command, channel, irc):
    try:
        message = message.split(' ')
        if len(message) - 1 == commandHeaders.commands[command]['argc']:
            return True
    except Exception:
        pp("Argument count is not defined!", "ERROR")
        epp("An internal error occurred! A highly trained SWAT team of Capybaras has been dispatched to terminate"
            "the error!", irc, channel)


def check_returns_function(command):
    if commandHeaders.commands[command]['return'] == 'command':
        return True


def pass_to_function(command, args, irc, channel, config, user):
    finalCommand = command.replace('#', '').replace('!', '')

    module = importlib.import_module('net.toffster.commandHandling.commands.%s' % finalCommand)
    function = getattr(module, finalCommand)

    #TODO: Create getters for modular use inside the commands' individual files
    if args:
        pp("Ran associated function with arguments." "DEBUG")
        return function(args, irc, channel, config, user)
    else:
        pp("Ran associated function without arguments.", "DEBUG")
        return function(irc, channel, irc.get_names_list(), config, user)


def handleComplexArgs(command, irc, channel):
    try:
        if "addcom" in command or "editcom" in command:
            commandSplit = command.split(' ')

            commandWord = commandSplit[0]
            operationTarget = commandSplit[1]
            operation = ""
            for operationWord in commandSplit[2:len(commandSplit)]:
                operation = operation + operationWord + ","

            return commandWord + " " + operationTarget + " " + operation

        else:
            return command

    except Exception:
        pp("Argument count is not defined!", "ERROR")
        epp(
            "An internal error occurred! A highly trained SWAT team of Capybaras has been dispatched to terminate the error!",
            irc, channel)


def add_to_viewer_data(name):
    try:
        with open("../res/viewerData.txt", "a") as trustValueStore:
            trustValueStore.write(name + " 0 0 0\n")
        return True
    except Exception as ex:
        pp("Error occurred when writing to viewer data file: " + str(ex) + "!", "ERROR")
        return False


def check_viewer_data(name):
    with open("../res/viewerData.txt", "r") as trustValueStore:
        temp_data = trustValueStore.read()

    temp_data_List = temp_data.split("\n")

    for line in temp_data_List[:-1]:
        if name == line.split(' ')[0]:
            return True

    return False


def get_viewer_trust(name):
    with open("../res/viewerData.txt", "r") as trustValueStore:
        temp_data = trustValueStore.read()

    temp_data = temp_data.split('\n')

    for line in temp_data:
        if name == line.split(' ')[0]:
            return line.split(' ')[1]

    return False


def get_viewer_permit(name):
    with open("../res/viewerData.txt", "r") as trustValueStore:
        temp_data = trustValueStore.read()

    temp_data = temp_data.split('\n')

    for line in temp_data:
        if name == line.split(' ')[0]:
            return line.split(' ')[2]

    return False


def set_viewer_trust(name, value):
    with open("../res/viewerData.txt", "r") as trustValueStore:
        data = trustValueStore.read()

    data = data.split('\n')
    new_data = ""

    for line in data:
        if name == line.split(' ')[0]:
            lineSplit = line.split(' ')
            lineSplit[1] = value

            line = ""
            for word in lineSplit:
                line += word + " "
            line = line[:-1]

        new_data += line + "\n"
    new_data = new_data[:-1]

    with open("../res/viewerData.txt", "w") as trustValueStore:
        trustValueStore.write(new_data)


def set_viewer_permit(name, value):
    with open("../res/viewerData.txt", "r") as trustValueStore:
        data = trustValueStore.read()

    data = data.split('\n')
    new_data = ""

    for line in data:
        if name == line.split(' ')[0]:
            lineSplit = line.split(' ')
            lineSplit[2] = value

            line = ""
            for word in lineSplit:
                line += word + " "
            line = line[:-1]

        new_data += line + "\n"
    new_data = new_data[:-1]

    with open("../res/viewerData.txt", "w") as trustValueStore:
        trustValueStore.write(new_data)


def get_viewer_points(name):
    with open("../res/viewerData.txt", "r") as trustValueStore:
        temp_data = trustValueStore.read()

    temp_data = temp_data.split('\n')

    for line in temp_data:
        if name == line.split(' ')[0]:
            return line.split(' ')[3]

    return False


def set_viewer_points(name, value):
    with open("../res/viewerData.txt", "r") as trustValueStore:
        data = trustValueStore.read()

    data = data.split('\n')
    new_data = ""

    for line in data:
        if name == line.split(' ')[0]:
            lineSplit = line.split(' ')
            lineSplit[3] = value

            line = ""
            for word in lineSplit:
                line += word + " "
            line = line[:-1]

        new_data += line + "\n"
    new_data = new_data[:-1]

    with open("../res/viewerData.txt", "w") as trustValueStore:
        trustValueStore.write(new_data)