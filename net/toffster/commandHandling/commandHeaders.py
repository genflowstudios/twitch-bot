#Import libraries
import net.toffster.data.config as configFile

#Command headers
commands = {}


def filterValue(value):
    value = value[1:].replace('\n', '')
    return value


def getDataFile():
    with open("../res/dataStore.txt", "r") as dataFile:
        for line in dataFile:
            if not line == "" and not line == None and len(line) > 5:
                (commandName, limit, returnValue, argc) = line.split(',')
                commands[commandName] = {}
                commands[commandName]['limit'] = int(filterValue(limit))
                commands[commandName]['return'] = filterValue(returnValue)
                commands[commandName]['argc'] = int(filterValue(argc))

    for command in commands:
        commands[command]['last_used'] = 0

getDataFile()