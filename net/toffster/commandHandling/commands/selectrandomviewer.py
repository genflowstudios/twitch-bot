import net.toffster.commandHandling.systemCommands as systemCommand
from random import randint

def selectrandomviewer(irc, channel, namesList, config, user):
    try:
        randomInt = randint(0, len(namesList) - 1)
        selected = namesList[randomInt]

        systemCommand.pp("Potential winners are: ", "INFO")
        for name in namesList:
            systemCommand.pp(name, "INFO")

        return "The winner is... " + selected

    except Exception as ex:
        systemCommand.pp(ex)
        systemCommand.pp("Exception occurred in selectrandomviewer.py", "ERROR")
        systemCommand.epp("An internal error occurred! A highly trained SWAT team of "
                           "Capybaras has been dispatched to terminate the error!", irc, channel)