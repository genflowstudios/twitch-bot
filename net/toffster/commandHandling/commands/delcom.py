import net.toffster.commandHandling.systemCommands as systemCommand
import net.toffster.commandHandling.commandHeaders as commandHeaders


def delcom(args, irc, channel, config, user):

    try:
        if args[0] in commandHeaders.commands:
            with open("../res/dataStore.txt", "r") as dataStore:
                data = dataStore.read()

            data = data.split("\n")
            newData = ""
            for line in data:
                if not args[0] in line.split(",")[0]:
                    newData += line + "\n"

            with open("../res/dataStore.txt", "w") as dataStore:
                dataStore.write(newData[:-1])

            commandHeaders.getDataFile()

            return "Command deleted successfully!"

        else:
            return "Command doesn't exist! To create use !addcom"

    except:
        systemCommand.pp("Error occurred when deleting command!", "ERROR")
        systemCommand.epp("Error when deleting command, please check you used the command correctly!", irc, channel)
        return "Error when deleting command, please check you used the command correctly!"