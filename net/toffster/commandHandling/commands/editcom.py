import net.toffster.commandHandling.systemCommands as systemCommand
import net.toffster.commandHandling.commandHeaders as commandHeaders


def editcom(args, irc, channel, config, user):

    try:
        if args[0] in commandHeaders.commands:
            with open("../res/dataStore.txt", "r") as dataStore:
                data = dataStore.read()

            data = data.split("\n")
            newData = ""
            for line in data:
                if args[0] in line.split(",")[0]:
                    lineSplit = line.split(",")
                    line = ""
                    lineSplit[2] = args[1].replace(",", "")

                    for word in lineSplit:
                        line += word

                newData += line + "\n"

            with open("../res/dataStore.txt", "w") as dataStore:
                dataStore.write(newData[:-1])

            commandHeaders.getDataFile()

            return "Command edited successfully!"

        else:
            return "Command doesn't exist! To create use !addcom"

    except:
        systemCommand.pp("Error occurred when editing command!", "ERROR")
        systemCommand.epp("Error when editing command, please check you used the command correctly!", irc, channel)
        return "Error when editing command, please check you used the command correctly!"