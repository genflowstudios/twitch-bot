import net.toffster.commandHandling.systemCommands as systemCommand
import net.toffster.commandHandling.commandHeaders as commandHeader


def commands(irc, channel, namesList, config, user):
    try:
        systemCommand.epp("The available commands are: ", irc, channel)

        commandList = ""
        firstRun = True

        for command in commandHeader.commands:
            if firstRun == True:
                filteredCommand = command.replace("!", "").replace("#", "")
                firstRun = False

                commandList = commandList + filteredCommand
            else:
                filteredCommand = command.replace("!", ", ").replace("#", ", ")
                if not filteredCommand in commandList:
                    commandList = commandList + filteredCommand

        return commandList

    except Exception:
        systemCommand.pp("Exception occurred in commands.py", "ERROR")
        systemCommand.epp("An internal error occurred! A highly trained SWAT team of "
                           "Capybaras has been dispatched to terminate the error!", irc, channel)