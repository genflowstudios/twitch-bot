import net.toffster.commandHandling.systemCommands as systemCommand


def points(irc, channel, namesList, config, user):
    return user + " currently has " + systemCommand.get_viewer_points(user) + " LUFFS! That's " + str(calcTime(config, user)) + " hours!"

def calcTime(config, user):
    return round(int(systemCommand.get_viewer_points(user)) / ((3600 / int(config['pointsInterval'])) * int(config['pointsPer'])), 2)