import net.toffster.commandHandling.systemCommands as systemCommand
import net.toffster.commandHandling.commandHeaders as commandHeaders


def addcom(args, irc, channel, config, user):
    try:
        if not args[0] in commandHeaders.commands:

            if not "!" in args[0] and not "#" in args[0]:
                args[0] = "!" + args[0]

            newCommand = args[0] + ", " + "30, " + args[1].replace(",", " ")[:-1] + ", 0" + "\n"

            with open("../res/dataStore.txt", "a") as dataStore:
                dataStore.write(newCommand)

            commandHeaders.getDataFile()

            return "Command added successfully!"

        else:
            return "Command already exists! To edit use !edtitcom"

    except:
        systemCommand.pp("Error occurred when adding command!", "ERROR")
        systemCommand.epp("Error when adding command, please check you used the command correctly!", irc, channel)
        return "Error when adding command, please check you used the command correctly!"