import net.toffster.commandHandling.systemCommands as systemCommand


def leaderboard(irc, channel, namesList, config, user):
    try:
        with open("../res/viewerData.txt") as viewerDataStore:
            viewerData = viewerDataStore.read()

        viewerData = viewerData.split('\n')[:-1]
        boardList = []

        for i in range(config['leaderboardSize']):
            tempScore = 0
            tempViewer = ""
            for i in range(len(viewerData) - 1):
                if int(viewerData[i].split(' ')[3]) > tempScore:
                    tempScore = int(viewerData[i].split(' ')[3])
                    tempViewer = viewerData[i].split(' ')[0]
                    del[viewerData[i]]

            boardList.append(tempViewer + " " + str(tempScore))

        i = 0

        for item in boardList:
            i += 1
            systemCommand.epp(str(i) + ". " + item.split(' ')[0] + ": " + item.split(' ')[1], irc, channel)

        return ""

    except Exception as ex:
        systemCommand.pp("Exception: " + str(ex) + " occurred when running leaderboard command!", "ERROR")