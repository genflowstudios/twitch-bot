import net.toffster.commandHandling.systemCommands as systemCommand

import threading
from time import sleep


def permit(args, irc, channel, config, user):
    try:
        systemCommand.set_viewer_permit(args[0], 1)

        temp_permit_thread = threading.Thread(target=run, args=(args, config,))
        temp_permit_thread.daemon = True
        temp_permit_thread.start()

        return "User: " + args[0] + "has been permitted to post links for the next " + str(config['permit_timeout']) + " seconds!"

    except Exception as ex:
        systemCommand.pp("The following exception occurred: " + str(ex), "ERROR")


def run(args, config):
    sleep(config['permit_timeout'])

    systemCommand.set_viewer_permit(args[0], 0)