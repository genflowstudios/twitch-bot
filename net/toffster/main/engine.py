import net.toffster.lib.irc as irc
import net.toffster.commandHandling.systemCommands as systemCommand
import net.toffster.lib.chatterBot as cB
import net.toffster.lib.voice as voice_lib
import net.toffster.lib.points as points
import net.toffster.lib.gui as gui
import net.toffster.lib.api as api
import net.toffster.lib.namesListHandler as nLH

import threading
from time import sleep


class BotEngine:
    def __init__(self, config):

        self.config = config
        self.irc = None
        self.socket = None
        self.bot_factory = cB.ChatterBotFactory()
        self.chat_bot = self.bot_factory.create(cB.ChatterBotType.CLEVERBOT)
        self.chat_bot_session = self.chat_bot.create_session()
        self.api = api.Channels(api.Utils())

        self.engineThread = threading.Thread(name="engineThread", target=self.run)
        self.engineThread.daemon = True
        self.engineThread.start()

        self.gui = self.init_gui()
        self.gui.main_loop()

    def init_gui(self):
        try:
            systemCommand.pp("Initialising GUI...", "INFO")
            temp = gui.GUI()
            systemCommand.cpp("Initialised GUI!", temp, "INFO")
            return temp
        except Exception as ex:
            systemCommand.pp("Error: " + str(ex) + " occurred when creating the GUI!", "ERROR")

    def run(self):

        while not gui.GUI.completionCheck:
            systemCommand.pp("Waiting for GUI init...", "DEBUG")
            sleep(1)
        sleep(1)

        self.irc = irc.Irc(self.config, self.gui)
        self.socket = self.irc.get_irc_socket_object

        local_irc = self.irc
        sock = self.socket
        config = self.config.config

        try:
            systemCommand.cpp("Initialising viewer list handler...", self.gui, "INFO")
            nLH.namesListHandler(config, local_irc, self.gui)
            systemCommand.cpp("Initialised viewer list handler!", self.gui, "INFO")
        except Exception as ex:
            systemCommand.cpp("Error: " + str(ex) + " occurred when creating viewer list handler!", self.gui, "ERROR")

        try:
            systemCommand.cpp("Initialising voice listener...", self.gui, "INFO")
            voice_lib.SpeechRecog(local_irc, config['channels'][0])
            systemCommand.cpp("Initialised voice listener!", self.gui, "INFO")
        except Exception as ex:
            systemCommand.cpp("Error: " + str(ex) + " occurred when creating voice listener!", self.gui, "ERROR")

        try:
            systemCommand.cpp("Initialising points system...", self.gui, "INFO")
            points.Points(config, local_irc, self.gui)
            systemCommand.cpp("Initialised points system!", self.gui, "INFO")
        except Exception as ex:
            systemCommand.cpp("Error: " + str(ex) + " occurred when starting points system!", self.gui, "ERROR")

        #SAVE: Twitch API
        # try:
        #     systemCommand.cpp("Attempting to connect to Twitch API...", self.gui, "INFO")
        #     channelObj = self.api.get_channel(name=config['channels'][0].replace('#', ''))
        #     systemCommand.cpp("Fetched channel object from Twitch API successfully!", self.gui, "INFO")
        # except Exception as ex:
        #     systemCommand.cpp("Error: " + str(ex) + " occurred when connecting to Twitch API", self.gui, "ERROR")

        while True:
            data = sock.recv(config['socket_buffer_size']).rstrip()

            if len(ascii(data)) == 0:
                try:
                    systemCommand.cpp('Connection was lost, reconnecting...', self.gui, "INFO")
                    sock = self.irc.get_irc_socket_object()
                except:
                    pass

            systemCommand.cpp(data.decode(), self.gui, 'DEBUG')

            # Check  for ping (reply)
            local_irc.check_for_ping(data)

            if local_irc.check_for_message(data):
                message_dict = local_irc.get_message(data, local_irc, config, self.chat_bot_session)

                channel = message_dict['channel']
                message = message_dict['message']
                username = message_dict['username']

                systemCommand.cpp("[" + username + "]" + " " + message, self.gui, "MSG")

                # Check if received message is a valid command and process
                if systemCommand.is_valid_command(message.lower()) or systemCommand.is_valid_command(message.split(' ')[0].lower()):
                    if message:
                        command = message
                    else:
                        systemCommand.cpp("Argument count is not defined!", self.gui, "ERROR")
                        systemCommand.epp(
                            "An internal error occurred! A highly trained SWAT team of Capybaras has been dispatched to terminate"
                            "the error!", local_irc, channel)
                        break

                    if systemCommand.check_returns_function(message.split(' ')[0].lower()):
                        command = systemCommand.handleComplexArgs(command, local_irc, channel)
                        if systemCommand.check_has_correct_args(command.lower(), command.split(' ')[0], channel, local_irc):
                            args = command.split(' ')
                            del args[0]

                            command = command.split(' ')[0].lower()

                            if systemCommand.is_on_cooldown(command):
                                systemCommand.cpp('Command is on cooldown. (%s) (%s) (%ss remaining)' % (
                                    command, username, systemCommand.get_cooldown_remaining(command)), self.gui, "INFO")
                            else:
                                systemCommand.cpp('Command is valid and not on cooldown. (%s) (%s)' % (
                                    command, username), self.gui, "INFO")

                                result = systemCommand.pass_to_function(command, args, local_irc, channel, config, username)
                                systemCommand.update_last_used(command)

                                if result:
                                    resp = ' > %s' % result
                                    systemCommand.cpp(resp, self.gui, "INFO")
                                    local_irc.send_message(channel, resp)
                                elif result is None or result == "":
                                    systemCommand.cpp("Null value returned by pass_to_function", self.gui, "ERROR")
                                    systemCommand.epp("An internal error occurred! A highly trained SWAT team of "
                                                      "Capybaras has been dispatched to terminate the error!", local_irc,
                                                      channel)
                                else:
                                    systemCommand.cpp(result, self.gui, "DEBUG")
                                    systemCommand.cpp("Unknown error after attempting to send function result", self.gui, "ERROR")
                                    systemCommand.epp("A completely unknown error has occurred! Oops...", local_irc, channel)

                    else:
                        if systemCommand.is_on_cooldown(command):
                            systemCommand.cpp('Command is on cooldown. (%s) (%s) (%ss remaining)' % (
                                command, username, systemCommand.get_cooldown_remaining(command)), self.gui, "INFO")
                        elif systemCommand.check_has_return(command):
                            systemCommand.cpp('Command is valid and not on cooldown. (%s) (%s)' % (
                                command, username), self.gui, "INFO")
                            systemCommand.update_last_used(command)

                            resp = ' > %s' % (systemCommand.get_return(command))
                            systemCommand.update_last_used(command)

                            systemCommand.cpp(resp, self.gui, "INFO")
                            local_irc.send_message(channel, resp)