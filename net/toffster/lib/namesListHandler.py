import threading
from time import sleep
import requests
from tkinter import END

import net.toffster.commandHandling.systemCommands as systemCommand


class namesListHandler:

    def __init__(self, config, irc, gui):
        self.irc = irc
        self.gui = gui
        self.guiList = gui.get_namesList()

        viewerListThread = threading.Thread(name="ViewerList", target=self.fetch_names_list, args=(config,))
        viewerListThread.daemon = True
        viewerListThread.start()

    def fetch_names_list(self, config):
        try:
            while True:
                sleep(10)
                oldNamesList = self.irc.get_names_list()

                data = requests.get('http://tmi.twitch.tv/group/user/' + config['channels'][0][1:] + '/chatters')

                viewers = data.json()['chatters']['viewers']
                mods = data.json()['chatters']['moderators']
                staff = data.json()['chatters']['admins'] + data.json()['chatters']['global_mods'] + data.json()['chatters']['staff']

                self.irc.set_names_list(viewers + mods)

                self.compare_name_lists(oldNamesList, self.irc.get_names_list())

                systemCommand.cpp("Updated viewer list!", self.gui, "DEBUG")

                if staff:
                    guiList = "Staff \n"
                    for staffMember in staff:
                        guiList += staffMember + '\n'

                    guiList += "\nModerators: \n"

                guiList = "Moderators: \n"
                for mod in mods:
                    guiList += mod + '\n'

                guiList += "\nViewers: \n"
                for viewer in viewers:
                    guiList += viewer + '\n'

                self.guiList.delete(0.0, END)
                self.guiList.insert(END, guiList)

        except Exception as ex:
            systemCommand.cpp(str(ex) + " in fetch_names_list", self.gui, "ERROR")

    def compare_name_lists(self, oldNamesList, newNamesList):
        try:
            for name in newNamesList:
                if name not in oldNamesList:
                    if not systemCommand.check_viewer_data(name):
                        tempCheck = systemCommand.add_to_viewer_data(name)
                        if tempCheck:
                            systemCommand.cpp("New viewer: " + name + " has joined the channel, added to data store!", self.gui, "INFO")

        except Exception as ex:
            systemCommand.cpp(str(ex) + " in compare_names_lists", self.gui, "ERROR")