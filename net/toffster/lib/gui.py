import net.toffster.commandHandling.systemCommands as systemCommand

import tkinter as tk
import pygubu
import threading


class GUI:
    completionCheck = False

    def __init__(self):
        self.builder = pygubu.Builder()

        self.builder.add_from_file("../res/mainGUI.ui")

        self.root = self.builder.get_object('root')
        self.console = self.builder.get_object('txt_console')
        self.namesList = self.builder.get_object('txt_list')

        systemCommand.pp(self.builder.objects, "DEBUG")

    def main_loop(self):
        GUI.completionCheck = True
        self.root.mainloop()

    def get_console(self):
        return self.console

    def get_namesList(self):
        return self.namesList