import net.toffster.commandHandling.systemCommands as systemCommand

from math import log

from whoosh.index import create_in
import whoosh.fields
from whoosh.fields import u
from whoosh.query import *

from tld import get_tld
from tld.utils import update_tld_names

class AntiSpam:
    def __init__(self):
        self.trust_store_data = self.fetch_file_data("viewerData.txt")
        self.forbidden_words = self.fetch_file_data("forbiddenWords.txt")
        self.recog = Recogniser()
        self.whoosh_schema = whoosh.fields.Schema(content=whoosh.fields.TEXT)
        self.whoosh_index = create_in("../res/whooshIndex", self.whoosh_schema)

        self.whoosh_writer = self.whoosh_index.writer()
        systemCommand.pp("Creating updated index...", "INFO")
        self.whoosh_writer.add_document(content=u(self.fetch_file_data("dict.txt")))
        systemCommand.pp("Index created!", "INFO")
        systemCommand.pp("Committing index...", "INFO")
        self.whoosh_writer.commit()
        systemCommand.pp("Index committed!", "INFO")

        update_tld_names()

    def fetch_file_data(self, file_name):
        with open("../res/" + file_name) as file:
            file_data = file.read()
        return file_data

    def categorise_message(self, message, irc, channel, name):
        goodList, badList = self.populateLists(message.lower().split(' '))
        recogWordRating = len(goodList) - len(badList)
        linkCheck = self.recog.check_for_links(message)

        systemCommand.pp("Link in message: " + str(linkCheck), "DEBUG")
        systemCommand.pp("Recognisable rating for the last message was: " + str(recogWordRating), "DEBUG")

        if linkCheck and systemCommand.get_viewer_permit(name) == 0:
            irc.send_message(channel, ".timeout " + name + " 1")
            systemCommand.epp("No links in chat! Your message has been deleted, however you have NOT been muted!", irc, channel)
        elif recogWordRating < -7:
            irc.send_message(channel, ".timeout " + name + " 1")
            systemCommand.epp("No spam in chat! Your message has been deleted, however you have NOT been muted!", irc, channel)

    def populateLists(self, messageList):
        goodList = []
        badList = []

        with self.whoosh_index.searcher() as searcher:
            for word in messageList:
                if not searcher.search(Term("content", word)):
                    temp = self.recog.infer_spaces(self.remove_special_characters(word.lower())).split(' ')
                    for processedWord in temp:
                        if searcher.search(Term("content", processedWord)):
                            goodList.append(processedWord)
                        else:
                            badList.append(processedWord)
                else:
                    goodList.append(word)

        return goodList, badList

    def remove_special_characters(self, word):
        characterList = ["/", "-", ".", ",", " ", "\\", "\"", "£", "$", "%", "^", "&", "*", "(", ")", "=", "+", "<", ">", "?"]

        for character in characterList:
            word = word.replace(character, "")

        return word


class Recogniser:
    def __init__(self):
        self.words = open("../res/freqDict.txt").read().split()
        self.word_cost = dict((k, log((i + 1) * log(len(self.words)))) for i, k in enumerate(self.words))
        self.max_word = max(len(x) for x in self.words)

    def infer_spaces(self, s):
        cost = [0]
        for i in range(1, len(s) + 1):
            c, k = self.best_match(i, s, cost)
            cost.append(c)

        out = []
        i = len(s)
        while i > 0:
            c, k = self.best_match(i, s, cost)
            assert c == cost[i]
            out.append(s[i - k:i])
            i -= k

        return " ".join(reversed(out))

    def best_match(self, i, s, cost):
            candidates = enumerate(reversed(cost[max(0, i - self.max_word):i]))
            return min((c + self.word_cost.get(s[i - k - 1:i], 9e999), k + 1) for k, c in candidates)

    def check_for_links(self, message):
        message = message.lower()
        message_list = message.split(' ')

        for word in message_list:
            if "." in word and word[len(word) - 1] != "." and word[0] != ".":
                if "http" not in word:
                    word = "http://" + word
                try:
                    get_tld(word, fail_silently=False, active_only=True)
                    return True
                except Exception as ex:
                    return False

        return False