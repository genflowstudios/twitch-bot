# Import libraries
import socket
import re
import sys
import threading
import requests
from time import sleep

import net.toffster.commandHandling.systemCommands as systemCommand
import net.toffster.lib.cron as cronCommand
import net.toffster.lib.antiSpam as aS
import net.toffster.lib.api as api


# Define class
class Irc:
    #Initialise config
    def __init__(self, configFile, gui):
        self.configFile = configFile
        self.anti_spam = aS.AntiSpam()
        self.api = api.Channels(api.Utils())
        self.sock = None
        self.namesList = []
        self.welcomed = []
        self.gui = gui

    def check_for_message(self, data):
        if re.match(
                r'^:[a-zA-Z0-9_]+![a-zA-Z0-9_]+@[a-zA-Z0-9_]+(\.tmi\.twitch\.tv|\.testserver\.local) PRIVMSG #[a-zA-Z0-9_]+ :.+$',
                data.decode('utf-8')):
            return True

    def check_is_command(self, message, valid_commands):
        for command in valid_commands:
            if command == message:
                return True

    def check_for_connected(self, data):
        if re.match(r'^:.+ 001 .+ :connected to TMI$', ascii(data)):
            return True

    def check_for_ping(self, data):
        if data.decode()[0:4] == "PING":
            self.sock.send(bytes('PONG tmi.twitch.tv\r\n', 'UTF-8'))
            systemCommand.pp('PONG tmi.twitch.tv!', 'DEBUG')

    def get_message(self, data, irc, config, chat_bot):
        return_dict = {
            'channel': re.findall(r'^:.+![a-zA-Z0-9_]+@[a-zA-Z0-9_]+.+ PRIVMSG (.*?) :', data.decode('UTF-8'))[0],
            'username': re.findall(r'^:([a-zA-Z0-9_]+)!', data.decode('UTF-8'))[0],
            'message': re.findall(r'PRIVMSG #[a-zA-Z0-9_]+ :(.+)', data.decode('UTF-8'))[0]
        }

        #TODO: Turn this back on
        #self.anti_spam.categorise_message(return_dict['message'], irc, return_dict['channel'], return_dict['username'])

        if return_dict["username"] == "gavinx_x" and return_dict["username"] not in self.welcomed:
            if config['welcomeMode']:
                systemCommand.epp("Welcome the one and only: Gavinx_x!", irc, config["channels"][0])
            self.welcomed.append(return_dict["username"])

        elif return_dict["username"] == "toffster96" and return_dict["username"] not in self.welcomed:
            if config['welcomeMode']:
                systemCommand.epp("Greetings master TOFFster! How may I assist you?", irc, config["channels"][0])
            self.welcomed.append(return_dict["username"])

        elif return_dict["username"] == "lavendercourage" and return_dict["username"] not in self.welcomed:
            if config['welcomeMode']:
                systemCommand.epp("All hail the most courageous of warriors! Lavendercourage!", irc, config["channels"][0])
            self.welcomed.append(return_dict["username"])

        elif return_dict["username"] not in self.welcomed:
            if not systemCommand.check_viewer_data(return_dict["username"]):
                systemCommand.add_to_viewer_data(return_dict["username"])
                if config['welcomeMode']:
                    systemCommand.epp("Welcome " + return_dict["username"] + " to the channel!", irc, config["channels"][0])
                self.welcomed.append(return_dict["username"])
            elif systemCommand.check_viewer_data(return_dict["username"]):
                if config['welcomeMode']:
                    systemCommand.epp("Welcome back to the stream " + return_dict["username"] + "!", irc, config["channels"][0])
                self.welcomed.append(return_dict["username"])

        if ("@luggage" in return_dict['message'].lower() or "@bot" in return_dict['message'].lower()) and config['chat_bot']:
            systemCommand.epp(chat_bot.think(return_dict['message'].replace('@luggagebot', '').replace('@luggage', '').replace('@bot', '')), irc, return_dict['channel'])

        return return_dict

    def check_login_status(self, data):
        if re.search('NOTICE \* :Login unsuccessful', data.decode('utf-8')):
            systemCommand.pp("Connection was terminated!", "ERROR")
            if self.configFile.config['debug']:
                systemCommand.pp(("Received: \n" + data.decode()[0:-2]), 'DEBUG')
            return False
        else:
            systemCommand.pp("Connection is open!", "INFO")
            if self.configFile.config['debug']:
                systemCommand.pp(("Received: \n" + data.decode()[0:-2]), 'DEBUG')
            return True

    def send_message(self, channel, message):
        finalMessage = bytes('PRIVMSG %s :%s\r\n' % (channel, message), 'UTF-8')
        systemCommand.cpp(finalMessage.decode()[0:-2], self.gui, 'DEBUG')
        self.sock.send(finalMessage)

    @property
    def get_irc_socket_object(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(10)

        self.sock = sock

        # noinspection PyBroadException
        try:
            sock.connect((self.configFile.config['server'], self.configFile.config['port']))
        except:
            systemCommand.pp('Cannot connect to server (%s:%s).' % (self.configFile.config['server'], self.configFile.config['port']), 'error')
            sys.exit()

        sock.settimeout(None)

        sock.send(bytes('USER %s\r\n' % self.configFile.config['username'], 'UTF-8'))
        sock.send(bytes('PASS %s\r\n' % self.configFile.config['oauth_password'], 'UTF-8'))
        sock.send(bytes('NICK %s\r\n' % self.configFile.config['username'], 'UTF-8'))

        if self.check_login_status(sock.recv(1024)):
            systemCommand.pp('Login successful.')
        else:
            systemCommand.pp('Login failed, enable debugging for more information.', 'ERROR')
            sys.exit()

        #Create threads for channels with cron messages
        for channel in self.configFile.config['channels']:
            if channel in self.configFile.config['cron']:
                if self.configFile.config['cron'][channel]['run_cron']:
                    threading._start_new_thread(cronCommand.Cron(self, channel).run, ())

        self.join_channels(self.channels_to_string(self.configFile.config['channels']))

        return sock

    def channels_to_string(self, channel_list):
        return ','.join(channel_list)

    def join_channels(self, channels):
        systemCommand.pp('Joining channels %s.' % channels)
        self.sock.send(bytes('JOIN %s\r\n' % channels, 'UTF-8'))
        systemCommand.pp('Joined channels.')
        self.send_message(channels, '.me is now online!')

    def leave_channels(self, channels):
        systemCommand.pp('Leaving channels %s,' % channels)
        self.send_message(channels, '.me has left the channel.')
        self.sock.send(bytes('PART %s\r\n' % channels, 'UTF-8'))
        systemCommand.pp('Left channels.')

    def get_names_list(self):
        return self.namesList

    def set_names_list(self, namesList):
        self.namesList = namesList

    def check_for_invalid_names(self):
        for name in self.namesList:
            if "#" in name or "JOIN" in name or "PART" in name:
                return True
        return False