import net.toffster.commandHandling.systemCommands as systemCommand

from win32com.client import constants as _constants
import win32com.client
import pythoncom
import time
import threading
from win32com.client import gencache

gencache.EnsureModule('{C866CA3A-32F7-11D2-9602-00C04F8EE628}', 0, 5, 0)

_voice = win32com.client.Dispatch("SAPI.SpVoice")
_recognizer = win32com.client.Dispatch("SAPI.SpSharedRecognizer")
_listeners = []
_handlerQueue = []
_eventThread = None


class Listener(object):

    _all = set()

    def __init__(self, context, grammar, callback):
        self._grammar = grammar
        Listener._all.add(self)

        _handlerQueue.append((context, self, callback))
        _ensure_event_thread()

    def isListening(self):
        return self in Listener._all

    def stopListening(self):
        try:
            Listener._all.remove(self)
        except KeyError:
            return False

        self._grammar = None

        if not Listener._all:
            global _eventThread
            _eventThread = None

        return True


_ListenerBase = win32com.client.getevents("SAPI.SpSharedRecoContext")


class _ListenerCallback(_ListenerBase):

    def __init__(self, oobj, listener, callback):
        _ListenerBase.__init__(self, oobj)
        self._listener = listener
        self._callback = callback

    def OnRecognition(self, _1, _2, _3, Result):
        if self._listener and not self._listener.isListening():
            self.close()
            self._listener = None

        if self._callback and self._listener:
            newResult = win32com.client.Dispatch(Result)
            phrase = newResult.PhraseInfo.GetText()
            self._callback(phrase, self._listener)


def say(phrase):
    _voice.Speak(phrase)


def input(prompt=None, phraselist=None):

    def response(phrase, listener):
        if not hasattr(listener, '_phrase'):
            listener._phrase = phrase
        listener.stopListening()

    if prompt:
        print(prompt)

    if phraselist:
        listener = listenFor(phraselist, response)
    else:
        listener = listenForAnything(response)

    while listener.isListening():
        time.sleep(.1)

    return listener._phrase


def stopListening():
    listeners = set(Listener._all)
    returns = [l.stopListening() for l in listeners]
    return any(returns)


def isListening():
    return not not Listener._all


def listenForAnything(callback):
    return _startListening(None, callback)


def listenFor(phraselist, callback):
    return _startListening(phraselist, callback)


def _startListening(phraselist, callback):
    context = _recognizer.CreateRecoContext()
    grammar = context.CreateGrammar()

    if phraselist:
        grammar.DictationSetState(0)
        rule = grammar.Rules.Add("rule",
                                 _constants.SRATopLevel + _constants.SRADynamic, 0)
        rule.Clear()

        for phrase in phraselist:
            rule.InitialState.AddWordTransition(None, phrase)

        grammar.Rules.Commit()

        grammar.CmdSetRuleState("rule", 1)
        grammar.Rules.Commit()
    else:
        grammar.DictationSetState(1)

    return Listener(context, grammar, callback)


def _ensure_event_thread():
    global _eventThread
    if not _eventThread:
        def loop():
            while _eventThread:
                pythoncom.PumpWaitingMessages()
                if _handlerQueue:
                    (context, listener, callback) = _handlerQueue.pop()
                    _ListenerCallback(context, listener, callback)
                time.sleep(.5)

        _eventThread = 1
        _eventThread = threading.Thread(name="eventThread", target=loop)
        _eventThread.daemon = True
        _eventThread.start()


class SpeechRecog:
    def __init__(self, irc, channel):
        self.irc = irc
        self.channel = channel
        self.phraseList = ["bot timeout", "bot time out", "bot purge", "bot clear chat", "bot run"]
        self.phraseDetailList = [True, True, True, False, True]

        self.run()

    def levelOneCallback(self, phrase, listener):
        systemCommand.pp("Recieved phrase" + phrase, "DEBUG")

        for i in range(len(self.phraseList)):
            if phrase == self.phraseList[i]:
                if self.phraseDetailList[i]:
                    L2 = listenForAnything(self.levelTwoCallback)
                    systemCommand.pp("Created L2")
                    systemCommand.pp(L2, "DEBUG")
                else:
                    if phrase == "bot clear chat":
                        self.irc.send_message(self.channel, ".clear")
                        self.irc.send_message(self.channel, "Chat was cleared by voice command!")

    def levelTwoCallback(self):
        systemCommand.pp("Reached level 2 callback", "DEBUG")
        #TODO: instance variable to store L2 voice data?

    def run(self):
        say("Voice recognition started successfully. Warning, this is an experimental feature so expect bugs, glitches "
            "and hilarious errors.")
        L1 = listenFor(self.phraseList, self.levelOneCallback)
        systemCommand.pp("Created L1")
        systemCommand.pp(L1, "DEBUG")
