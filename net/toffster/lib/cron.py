# Import libraries
import time
import net.toffster.data.config as configFile
import net.toffster.commandHandling.systemCommands as systemCommand

#Define class
class Cron:

    #Initiliase messages, run time etc
    def __init__(self, irc, channel):
        self.messages = configFile.config['cron'][channel]['cron_messages']
        self.run_time = configFile.config['cron'][channel]['run_time']
        self.last_index = 0
        self.irc = irc
        self.channel = channel
        self.last_ran = None

    def get_next_message(self):
        next_index = self.last_index + 1

        if next_index > len(self.messages) - 1:
            next_index = 0

        self.last_index = next_index

        return next_index

    #Main method
    def run(self):
        time.sleep(self.run_time)
        while True:
            index = self.get_next_message()

            systemCommand.pp('[CRON] %s' % self.messages[index], "INFO")

            self.irc.send_message(self.channel, self.messages[index])

            self.last_ran = time.time()

            time.sleep(self.run_time)