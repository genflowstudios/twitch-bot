from time import sleep
import threading

import net.toffster.commandHandling.systemCommands as systemCommand


class Points:
    def __init__(self, config, irc, gui):
        self.namesList = irc.get_names_list()
        self.config = config
        self.irc = irc
        self.gui = gui

        pointsThread = threading.Thread(name="pointsThread", target=self.run)
        pointsThread.daemon = True
        pointsThread.start()

    def run(self):
        while True:
            try:
                sleep(self.config['pointsInterval'])

                systemCommand.cpp("Adding points...", self.gui, "INFO")
                self.namesList = self.irc.get_names_list()

                for user in self.namesList:
                    try:
                        userPoints = systemCommand.get_viewer_points(user)

                        if userPoints:
                            systemCommand.set_viewer_points(user, str(int(systemCommand.get_viewer_points(user)) + self.config['pointsPer']))
                        else:
                            systemCommand.cpp("User: " + user + " is not eligible for points!", self.gui, "ERROR")

                    except Exception as ex:
                        systemCommand.cpp("An error occurred when attempting to add points to user: " + user + " the exception"
                                         "was: " + str(ex), self.gui, "ERROR")

                systemCommand.cpp("Finished adding points!", self.gui, "INFO")
            except Exception as ex:
                systemCommand.cpp("An error occurred in the points loop: " + str(ex), self.gui, "ERROR")