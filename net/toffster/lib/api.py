import json
import requests

import net.toffster.commandHandling.systemCommands as systemCommand


class Channels():
    def __init__(self, utils):
        # Get provided base endpoints from the Twitch API
        r = requests.get('https://api.twitch.tv/kraken/')
        self.utils = utils
        # Check if the API is available
        if r.status_code != 200:
            systemCommand.pp("Twitch API returned error 200!", "ERROR")
        else:
            # Set BASE endpoints provided by Twitch for easy navigation
            self.base_endpoints = r.json()['_links']
            # Set BASE (default) header values
            self.headers = {
                'Accept': 'application/vnd.twitchtv.v3+json',
                'content-type': 'application/json'
            }
            # Set BASE payload (parameters) for set_channel() function
            self.payload = {'channel': {}}

    def get_channel(self, **kwargs):
        # Get kwargs and store them in a data dict
        data = self.utils.data(kwargs)

        if 'oauth' in data:
            # Set the OAuth header
            self.headers['Authorization'] = 'OAuth ' + data['oauth']
            # Get the OAuth channel object
            r = requests.get(self.base_endpoints['channel'], headers=self.headers)
        if 'name' in data:
            # Get channel object by name (public)
            r = requests.get(self.base_endpoints['channel'] + 's/' + data['name'], headers=self.headers)
        # Return Channel Object
        if r.status_code == 200:
            return r.json()
        elif r.status_code == 404:
            return r.json()

        # Return a error if none of the arguments was met
        return self.utils.error(error='Fatal Error', message='get_channel: See the Py Twitch documentation for usage.',
                                status=101)

    def set_channel(self, **kwargs):
        # Get kwargs and store them in a data dict
        data = self.utils.data(kwargs)

        if 'oauth' in data:
            # Set the OAuth header
            self.headers['Authorization'] = 'OAuth ' + data['oauth']
            # Get channel object by oauth
            r = requests.get(self.base_endpoints['channel'], headers=self.headers)
        else:
            self.utils.error(error='Unauthorized', message='OAuth is requried, see documentation', status=401)

        if 'title' in data or 'game' in data:
            # If title or game specified add to the payload
            if 'title' in data:
                self.payload['channel']['status'] = data['title']
            if 'game' in data:
                self.payload['channel']['game'] = data['game']
            # Send the data to be updated
            r = requests.put(self.base_endpoints['channel'], data=json.dumps(self.payload), headers=self.headers)
            # Return the updated channel object
            return r.json()

        # Return a error if none of the arguments was met
        return self.utils.error(error='Fatal Error', message='set_channel: See the PyTwtich documentation for useage.',
                                status=101)

    def get_editors(self, **kwargs):
        # Get kwargs and store them in a data dict
        data = self.utils.data(kwargs)

        if 'oauth' in data:
            # Set the OAuth header
            self.headers['Authorization'] = 'OAuth ' + data['oauth']
            r = requests.get(self.base_endpoints['channel'], headers=self.headers)
            # Check for errors
            if r.json()['status'] == 401:
                return r.json()
            # Set channel specific Twitch API endpoints
            self.channel_endpoints = r.json()['_links']

            # Get editors json object
            r = requests.get(self.channel_endpoints['editors'], headers=self.headers)
            return r.json()
        else:
            self.utils.error(error='Unauthorized', message='OAuth is requried, see documentation', status=401)

        # Return a error if none of the arguments was met
        return self.utils.error(error='Fatal Error', message='get_editors: See the PyTwtich documentation for useage.',
                                status=101)


class Utils():
    def data(self, kwargs):
        data = {}
        for key, value in kwargs.items():
            data[key] = value
        return data

    def error(self, **kwargs):
        error_temp = {}
        data = self.data(kwargs)
        if 'error' in data:
            error_temp['error'] = data['error']
        else:
            error_temp['error'] = ''
        if 'message' in data:
            error_temp['message'] = data['message']
        else:
            error_temp['message'] = ''
        if 'status' in data:
            error_temp['status'] = data['status']
        else:
            error_temp['status'] = ''
        return error_temp

    def pretty_header(self, text, *underline):
        if text and underline:
            underline = underline[0]
        else:
            underline = '='
        return print(text + '\n' + underline * len(text))

    def pretty_json(self, data):
        if data:
            return print(json.dumps(data, indent=4, sort_keys=True) + '\n')
        else:
            return print(self.error(error='No Data',
                                    message='You did not specify JSON data source to be pretty printed.',
                                    status=101))