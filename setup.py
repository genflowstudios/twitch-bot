from distutils.core import setup

setup(
    name='Twitch Bot',
    version='0.2.1a',
    packages=['net', 'net.toffster', 'net.toffster.lib', 'net.toffster.res', 'net.toffster.data', 'net.toffster.main',
              'net.toffster.commandHandling', 'net.toffster.commandHandling.commands'],
    url='',
    license='',
    author='TOFFster96',
    author_email='toffster96@gmail.com',
    description=''
)
